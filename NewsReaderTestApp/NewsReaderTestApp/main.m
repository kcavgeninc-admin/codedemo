//
//  main.m
//  NewsReaderTestApp
//
//  Created by Danekar, Koustubh S. (Contractor) on 2/10/16.
//  Copyright © 2016 Danekar, Koustubh S. (Contractor). All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
