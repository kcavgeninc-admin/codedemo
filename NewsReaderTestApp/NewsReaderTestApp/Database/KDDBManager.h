//
//  KDDBManager.h
//  NewsReaderTestApp
//
//  Created by Danekar, Koustubh S. (Contractor) on 2/13/16.
//  Copyright © 2016 Danekar, Koustubh S. (Contractor). All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KDDBManager : NSObject

@property (nonatomic, strong) NSMutableArray *arrColumnNames;

@property (nonatomic) int affectedRows;

@property (nonatomic) long long lastInsertedRowID;

- (BOOL)createDatabaseWithFileName:(NSString*)dbFilename sqlQuery:(NSString*)sqlQueryStmt;

- (instancetype)initWithDatabaseFilename:(NSString *)dbFilename;

- (NSArray *)loadDataFromDB:(NSString *)query;

- (void)executeQuery:(NSString *)query;

- (NSArray *) singleColumnQuery:(NSString *) sql;

- (NSArray *) multiColumnQuery:(NSString *) sql rowBlock:(id(^)( NSDictionary *rowData)) row;

@end
