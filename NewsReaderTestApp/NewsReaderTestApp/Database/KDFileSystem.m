//
//  KDFileSystem.m
//  NewsReaderTestApp
//
//  Created by Danekar, Koustubh S. (Contractor) on 2/13/16.
//  Copyright © 2016 Danekar, Koustubh S. (Contractor). All rights reserved.
//

#import "KDFileSystem.h"

@implementation KDFileSystem

+ (NSString *) getDocumentsDirectory
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    return [paths objectAtIndex:0];
}

+ (NSString *) getFilePathInDocumentsForResouce:(NSString *) resouceName
{
    return [[self getDocumentsDirectory] stringByAppendingPathComponent:resouceName];
//    [NSString stringWithFormat:@"%@/%@",[self getDocumentsDirectory],resouceName];
}


@end
