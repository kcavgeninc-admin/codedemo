//
//  KDFileSystem.h
//  NewsReaderTestApp
//
//  Created by Danekar, Koustubh S. (Contractor) on 2/13/16.
//  Copyright © 2016 Danekar, Koustubh S. (Contractor). All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KDFileSystem : NSObject

+ (NSString *) getDocumentsDirectory;

+ (NSString *) getFilePathInDocumentsForResouce:(NSString *) resouceName;

@end
