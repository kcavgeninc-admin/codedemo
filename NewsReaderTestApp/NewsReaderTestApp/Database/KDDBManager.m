//
//  KDDBManager.m
//  NewsReaderTestApp
//
//  Created by Danekar, Koustubh S. (Contractor) on 2/13/16.
//  Copyright © 2016 Danekar, Koustubh S. (Contractor). All rights reserved.
//

#import "KDDBManager.h"
#import <sqlite3.h>

@interface KDDBManager()
{
    NSString *databasePath;
}

@property (nonatomic, strong) NSString *documentsDirectory;

@property (nonatomic, strong) NSString *databaseFilename;

@property (nonatomic, strong) NSMutableArray *arrResults;


-(void)copyDatabaseIntoDocumentsDirectory;

-(void)runQuery:(const char *)query isQueryExecutable:(BOOL)queryExecutable;

@end


@implementation KDDBManager

#pragma mark - Creating Database

- (BOOL)createDatabaseWithFileName:(NSString*)dbFilename sqlQuery:(NSString*)sqlQueryStmt
{
    // Create a sqlite object.
    sqlite3 *sqlite3Database;
    
    BOOL isDatabaseCreatedSuccesfully;
    
    self.databaseFilename = dbFilename;
    
    // Build the path to the database file
    databasePath = [KDFileSystem getFilePathInDocumentsForResouce:self.databaseFilename];
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
    
    if ([filemgr fileExistsAtPath:databasePath] == NO)
    {
        const char *dbpath = [databasePath UTF8String];
        
        if (sqlite3_open(dbpath, &sqlite3Database) == SQLITE_OK)
        {
            char *errMsg;
            
            if (sqlite3_exec(sqlite3Database, [sqlQueryStmt UTF8String], NULL, NULL, &errMsg) != SQLITE_OK)
            {
                DLog(@"Failed to create table with error: %s", errMsg);
            }
            else{
                isDatabaseCreatedSuccesfully = YES;
            }
            
            sqlite3_close(sqlite3Database);
            
        } else {
            DLog(@"Failed to open/create database");
        }
    }
    
    return isDatabaseCreatedSuccesfully;
    
}

#pragma mark - Initialization

-(instancetype)initWithDatabaseFilename:(NSString *)dbFilename
{
    self = [super init];
    if (self) {
        
        // Keep the database filename.
        self.databaseFilename = dbFilename;
        
        // Build the path to the database file
        databasePath = [KDFileSystem getFilePathInDocumentsForResouce:self.databaseFilename];
        
        // Copy the database file into the documents directory if necessary.
        [self copyDatabaseIntoDocumentsDirectory];
    }
    return self;
}


#pragma mark - Private method implementation

-(void)copyDatabaseIntoDocumentsDirectory{
    
    // Check if the database file exists in the documents directory.
    if (![[NSFileManager defaultManager] fileExistsAtPath:databasePath]) {
        // The database file does not exist in the documents directory, so copy it from the main bundle now.
        NSString *sourcePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:self.databaseFilename];
        NSError *error;
        [[NSFileManager defaultManager] copyItemAtPath:sourcePath toPath:databasePath error:&error];
        
        // Check if any error occurred during copying and display it.
        if (error != nil) {
            DLog(@"%@", [error localizedDescription]);
        }
    }
}



-(void)runQuery:(const char *)query isQueryExecutable:(BOOL)queryExecutable{
    // Create a sqlite object.
    sqlite3 *sqlite3Database;
    
    // Set the database file path.
    NSString *databasePrivatePath = [KDFileSystem getFilePathInDocumentsForResouce:self.databaseFilename];
    
    // Initialize the results array.
    if (self.arrResults != nil) {
        [self.arrResults removeAllObjects];
        self.arrResults = nil;
    }
    self.arrResults = [[NSMutableArray alloc] init];
    
    // Initialize the column names array.
    if (self.arrColumnNames != nil) {
        [self.arrColumnNames removeAllObjects];
        self.arrColumnNames = nil;
    }
    self.arrColumnNames = [[NSMutableArray alloc] init];
    
    
    // Open the database.
    BOOL openDatabaseResult = sqlite3_open([databasePrivatePath UTF8String], &sqlite3Database);
    if(openDatabaseResult == SQLITE_OK) {
        // Declare a sqlite3_stmt object in which will be stored the query after having been compiled into a SQLite statement.
        sqlite3_stmt *compiledStatement;
        
        // Load all data from database to memory.
        if(sqlite3_prepare_v2(sqlite3Database, query, -1, &compiledStatement, NULL) == SQLITE_OK) {
            // Check if the query is non-executable.
            if (!queryExecutable){
                // In this case data must be loaded from the database.
                
                // Declare an array to keep the data for each fetched row.
                NSMutableArray *arrDataRow;
                
                // Loop through the results and add them to the results array row by row.
                while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
                    // Initialize the mutable array that will contain the data of a fetched row.
                    arrDataRow = [[NSMutableArray alloc] init];
                    
                    // Get the total number of columns.
                    int totalColumns = sqlite3_column_count(compiledStatement);
                    
                    // Go through all columns and fetch each column data.
                    for (int i=0; i<totalColumns; i++){
                        // Convert the column data to text (characters).
                        char *dbDataAsChars = (char *)sqlite3_column_text(compiledStatement, i);
                        
                        // If there are contents in the currenct column (field) then add them to the current row array.
                        if (dbDataAsChars != NULL) {
                            // Convert the characters to string.
                            [arrDataRow addObject:[NSString  stringWithUTF8String:dbDataAsChars]];
                        }
                        
                        // Keep the current column name.
                        if (self.arrColumnNames.count != totalColumns) {
                            dbDataAsChars = (char *)sqlite3_column_name(compiledStatement, i);
                            [self.arrColumnNames addObject:[NSString stringWithUTF8String:dbDataAsChars]];
                        }
                    }
                    
                    // Store each fetched data row in the results array, but first check if there is actually data.
                    if (arrDataRow.count > 0) {
                        [self.arrResults addObject:arrDataRow];
                    }
                }
            }
            else {
                // This is the case of an executable query (insert, update, ...).
                
                // Execute the query.
                if (sqlite3_step(compiledStatement) == SQLITE_DONE) {
                    // Keep the affected rows.
                    self.affectedRows = sqlite3_changes(sqlite3Database);
                    
                    // Keep the last inserted row ID.
                    self.lastInsertedRowID = sqlite3_last_insert_rowid(sqlite3Database);
                }
                else {
                    // If could not execute the query show the error message on the debugger.
                    DLog(@"DB Error: %s", sqlite3_errmsg(sqlite3Database));
                }
            }
        }
        else {
            // In the database cannot be opened then show the error message on the debugger.
            DLog(@"%s", sqlite3_errmsg(sqlite3Database));
        }
        
        // Release the compiled statement from memory.
        sqlite3_finalize(compiledStatement);
        
    }
    
    // Close the database.
    sqlite3_close(sqlite3Database);
}

- (id)valueForStatement:(sqlite3_stmt *)statement atIndex:(int)index {
    NSUInteger columnType = sqlite3_column_type(statement, index);
    id value = nil;
    switch (columnType) {
        case SQLITE_INTEGER:
            value = @(sqlite3_column_int(statement, index));
            break;
        case SQLITE_TEXT:
            value = @((char *)sqlite3_column_text(statement, index));
            break;
        case SQLITE_FLOAT:
            value = @(sqlite3_column_double(statement, index));
            break;
        case SQLITE_NULL:
        default:
            value = @"";
            break;
    }
    return value;
}

#pragma mark - Public method implementation

-(NSArray *)loadDataFromDB:(NSString *)query{
    // Run the query and indicate that is not executable.
    // The query string is converted to a char* object.
    [self runQuery:[query UTF8String] isQueryExecutable:NO];
    
    // Returned the loaded results.
    return (NSArray *)self.arrResults;
}


-(void)executeQuery:(NSString *)query{
    // Run the query and indicate that is executable.
    [self runQuery:[query UTF8String] isQueryExecutable:YES];
}

- (NSArray *) singleColumnQuery:(NSString *) sql {
    NSMutableArray *columnList = nil;
    
    sqlite3 *sqlite3Database;
    if (sqlite3_open([databasePath UTF8String], &sqlite3Database) == SQLITE_OK) {
        sqlite3_stmt *statement;
        if (sqlite3_prepare_v2(sqlite3Database, [sql UTF8String], -1, &statement, NULL) ==
            SQLITE_OK) {
            columnList = [NSMutableArray array];
            while (sqlite3_step(statement) == SQLITE_ROW) {
                
                [columnList addObject:[self valueForStatement:statement atIndex:0]];
            }
        }
        sqlite3_finalize(statement);
    }
    DLog(@"Query:%@\n Result:%@",sql,columnList);
    
    // Close the database.
    sqlite3_close(sqlite3Database);
    
    return columnList;
    
}

- (NSArray *) multiColumnQuery:(NSString *) sql rowBlock:(id(^)( NSDictionary *rowData)) row{
    NSMutableArray *rows = nil;
    sqlite3 *sqlite3Database;
    if (sqlite3_open([databasePath UTF8String], &sqlite3Database) == SQLITE_OK)
    {
        sqlite3_stmt *statement;
        if (sqlite3_prepare_v2(sqlite3Database, [sql UTF8String], -1, &statement, NULL) == SQLITE_OK) {
            rows = [NSMutableArray array];
            while (sqlite3_step(statement) == SQLITE_ROW) {
                NSMutableDictionary *columnDictionary = [NSMutableDictionary dictionary];
                for (int columnIndex = 0; columnIndex < sqlite3_column_count(statement); columnIndex++) {
                    [columnDictionary setValue:[self valueForStatement:statement atIndex:columnIndex] forKey:@((const char *)
                     sqlite3_column_name(statement, columnIndex))];
                }
                [rows addObject:row(columnDictionary)];
            }
        }
        sqlite3_finalize(statement);
    }
    
    // Close the database.
    sqlite3_close(sqlite3Database);
    
    return rows;
}



@end
