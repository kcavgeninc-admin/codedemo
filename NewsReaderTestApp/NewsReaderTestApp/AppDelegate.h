//
//  AppDelegate.h
//  NewsReaderTestApp
//
//  Created by Danekar, Koustubh S. (Contractor) on 2/10/16.
//  Copyright © 2016 Danekar, Koustubh S. (Contractor). All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

