//
//  NewsDataModel.m
//  NewsReaderTestApp
//
//  Created by Danekar, Koustubh S. (Contractor) on 2/11/16.
//  Copyright © 2016 Danekar, Koustubh S. (Contractor). All rights reserved.
//

#import "NewsDataModel.h"
#import "NewsHeaderModel.h"

@interface NewsDataModel()
{
    KDDBManager *dbManager;
    NSString *sqlTableName;
    NSUserDefaults *userdefaults;
}

@end

@implementation NewsDataModel
SYNTHESIZE_SINGLETON_FOR_CLASS(NewsDataModel);

- (id)init
{
    if (self==[super init]) {
        
        _dataSource = [NSMutableArray new];
         userdefaults = [NSUserDefaults standardUserDefaults];
        [self createDatabase];
    }
    
    return self;
}

- (void)createDatabase
{
    sqlTableName = @"NEWS_TABLE";
    NSString *sql_Stmt = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS '%@'('%@' INTEGER, '%@' TEXT, '%@' TEXT, '%@' TEXT, '%@' TEXT, '%@' TEXT, '%@' TEXT)", sqlTableName, @"REF_ID", @"HEADLINE", @"IMAGENAME", @"DATE", @"WEBSITE", @"LIKES_COUNT", @"SELECTION_STATE"];
    dbManager = [KDDBManager new];
    BOOL isDatabaseCreated = [dbManager createDatabaseWithFileName:@"NewsArticles.sqlite" sqlQuery:sql_Stmt];
    if (isDatabaseCreated) {
        
        DLog(@"Database created. Now next step");
        
    }
}

- (void)reloadData:(void(^)( BOOL isDataAvailable)) callback
{
    // ********* Data can be loaded by downloading from a server and binded to local datamodel.  hence mimicking that by reading it from array for demo purpose
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        if (_dataSource.count>0) {
            [_dataSource removeAllObjects];
        }
        
        //For now am using just one thumbnail image for all the headlines for DEMO purpose
        
        NSArray *imageNamesArray = [NSArray arrayWithObjects:@"politics-70", @"sports-70", @"business-70", @"science-70", @"art-70", @"music-70",  nil];
        
        BOOL isDatabasePopulated = [userdefaults boolForKey:kDatabasePopulated];
        
        //Logic is to keep downloading in the background and keep refreshed data ready to be displayed for better user experience
        NSArray *headerStringsArray = [NSArray arrayWithObjects:@"Somebody did something in politics today and this is first long header", @"A Sport thing happened", @"Business news event", @"Science news", @"Art Art Art!!", @"Check out this awesome band right now", nil];
        
        
        NSString *politics = @"http://www.cnn.com/";
        NSString *sportsSite = @"http://espn.go.com/";
        NSString *businessSite = @"http://www.foxbusiness.com/";
        NSString *scienceSite = @"http://mobile.nytimes.com/2016/02/12/science/ligo-gravitational-waves-black-holes-einstein.html?emc=edit_na_20160211&nl=bna&nlid=59111829&te=1&referer&_r=0";
        NSString *artsSite = @"http://fineartamerica.com/";
        NSString *musicSite = @"http://www.rollingstone.com/music/news";
        NSArray *sitesDataArray = [NSArray arrayWithObjects:politics, sportsSite, businessSite, scienceSite, artsSite, musicSite, nil];
        
        [headerStringsArray enumerateObjectsUsingBlock:^(NSString *aHeadline, NSUInteger idx, BOOL * _Nonnull stop) {
            
            NSString *sql_Stmt = nil;
            
            if (!isDatabasePopulated) {
                sql_Stmt = [NSString stringWithFormat:@"insert into %@ values('%ld', '%@', '%@', '%@', '%@', '%@', '%@')", sqlTableName, (long)idx, aHeadline, [imageNamesArray objectAtIndex:idx], [NSString stringWithFormat:@"%@", [self getFormattedDate:[NSDate date]]], [sitesDataArray objectAtIndex:idx], [NSString stringWithFormat:@"%ld", (long)idx+10], @"Un-Selected"];
                
                [dbManager executeQuery:sql_Stmt];
            }
//            else{
//                sql_Stmt = [NSString stringWithFormat:@"update %@ set HEADLINE= '%@', IMAGENAME= '%@', DATE= '%@', WEBSITE= '%@' where REF_ID= %ld", sqlTableName, aHeadline, [imageNamesArray objectAtIndex:0], [NSString stringWithFormat:@"%@", [self getFormattedDate:[NSDate date]]], [sitesDataArray objectAtIndex:idx], (long)idx];
//            }
            
            
            
            
        }];
        
        
        [userdefaults setBool:YES forKey:kDatabasePopulated];
        
        callback(YES);
    });
    
    
}

- (NSString *)getFormattedDate:(NSDate *)localDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"dd-MMM-yyyy hh:mm:ss a"];
    
    NSString *dateString = [dateFormatter stringFromDate:localDate];
    
    return dateString;
}


- (NSArray *)allNewsArticles
{
    //Now query into database and populate our UI binding datamodel
    // Form the query.
    NSString *sql = [NSString stringWithFormat:@"SELECT HEADLINE, IMAGENAME, DATE, WEBSITE, LIKES_COUNT, SELECTION_STATE FROM '%@'", sqlTableName];
    NSArray *dbContentArray = [dbManager multiColumnQuery:sql rowBlock:^(NSDictionary *rowData) {
        DLog(@"rowData: %@", rowData);
        NewsHeaderModel *newsModel = [[NewsHeaderModel alloc] initWithDictionary:rowData];
        return newsModel;
    }];

    return dbContentArray;
}

- (void)incrementLikesCountOf:(NewsHeaderModel*)newsDataModel AtIndex:(NSInteger)index
{
    NSString *sql_Stmt = [NSString stringWithFormat:@"update %@ set LIKES_COUNT= '%@', SELECTION_STATE = '%@' where REF_ID= %ld", sqlTableName, [NSString stringWithFormat:@"%ld", [newsDataModel.LIKES_COUNT integerValue]], @"Selected", (long)index];
    [dbManager executeQuery:sql_Stmt];
    
}

- (void)decrementLikesCountOf:(NewsHeaderModel*)newsDataModel AtIndex:(NSInteger)index
{
    NSString *sql_Stmt = [NSString stringWithFormat:@"update %@ set LIKES_COUNT= '%@', SELECTION_STATE = '%@' where REF_ID= %ld", sqlTableName, [NSString stringWithFormat:@"%ld", [newsDataModel.LIKES_COUNT integerValue]], @"Un-Selected", (long)index];
    [dbManager executeQuery:sql_Stmt];
    
}




@end
