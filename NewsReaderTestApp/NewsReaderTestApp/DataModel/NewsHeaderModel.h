//
//  NewsHeaderModel.h
//  NewsReaderTestApp
//
//  Created by Danekar, Koustubh S. (Contractor) on 2/10/16.
//  Copyright © 2016 Danekar, Koustubh S. (Contractor). All rights reserved.
//


@interface NewsHeaderModel : NSObject

@property(nonatomic, strong) NSString *HEADLINE;
@property(nonatomic, strong) NSString *IMAGENAME;
@property(nonatomic, strong) NSString *DATE;
@property(nonatomic, strong) NSString *WEBSITE;
@property(nonatomic, assign) NSString *LIKES_COUNT;
@property(nonatomic, strong) NSString *SELECTION_STATE;


- (instancetype) initWithDictionary:(NSDictionary *) dictionary;

@end
