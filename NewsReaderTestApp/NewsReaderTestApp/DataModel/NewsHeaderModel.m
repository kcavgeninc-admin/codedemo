//
//  NewsHeaderModel.m
//  NewsReaderTestApp
//
//  Created by Danekar, Koustubh S. (Contractor) on 2/10/16.
//  Copyright © 2016 Danekar, Koustubh S. (Contractor). All rights reserved.
//


#import "NewsHeaderModel.h"

@implementation NewsHeaderModel

- (instancetype) initWithDictionary:(NSDictionary *) dictionary {
    self = [self init];
    if (self) {
        self.HEADLINE = dictionary[@"HEADLINE"];
        self.IMAGENAME = dictionary[@"IMAGENAME"];
        self.DATE = dictionary[@"DATE"];
        self.WEBSITE = dictionary[@"WEBSITE"];
        self.LIKES_COUNT = [dictionary valueForKey:@"LIKES_COUNT"];
        self.SELECTION_STATE = dictionary[@"SELECTION_STATE"];
    }
    return self;
}

@end
