//
//  NewsDataModel.h
//  NewsReaderTestApp
//
//  Created by Danekar, Koustubh S. (Contractor) on 2/11/16.
//  Copyright © 2016 Danekar, Koustubh S. (Contractor). All rights reserved.
//

#import "NewsHeaderModel.h"

@interface NewsDataModel : NSObject
SYNTHESIZE_SINGLETON_FOR_CLASS_HEADER(NewsDataModel);

@property(nonatomic, strong) NSMutableArray *dataSource;

- (void)reloadData:(void(^)( BOOL isDataAvailable)) callback;

- (NSArray *)allNewsArticles;

- (void)incrementLikesCountOf:(NewsHeaderModel*)newsDataModel AtIndex:(NSInteger)index;

- (void)decrementLikesCountOf:(NewsHeaderModel*)newsDataModel AtIndex:(NSInteger)index;

@end
