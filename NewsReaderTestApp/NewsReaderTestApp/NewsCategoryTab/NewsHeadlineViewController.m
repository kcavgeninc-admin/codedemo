//
//  NewsHeadlineViewController.m
//  NewsReaderTestApp
//
//  Created by Danekar, Koustubh S. (Contractor) on 2/10/16.
//  Copyright © 2016 Danekar, Koustubh S. (Contractor). All rights reserved.
//

#import "NewsHeadlineViewController.h"
#import "NewsHeaderTableCell.h"
#import "MBProgressHUD.h"
#import "NewsDataModel.h"
#import "NewsHeaderModel.h"
#import "DetailViewController.h"

@interface NewsHeadlineViewController ()<UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate>
{
    NSInteger countOfRows;
    __weak IBOutlet UISearchBar *_searchBar;
    CGRect originalSearchBarRect;
    NSArray *defaultDataContentArray;
}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchBarTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchBarHeighConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableviewTopContraint;

@property (weak, nonatomic) IBOutlet UITableView *headlineTableView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *searchButton;
@property (nonatomic) BOOL displayingSearchResults;
@property (strong, nonatomic) NSArray *transientDataSource;
;

@end

/********************************

 For Demo purpose I have chosen SQLite which can just as easily implemented using Core-Data.  Also all the resources used in the project are ONLY for demo purpose and obtained from Clipart.
 
*********************************/

@implementation NewsHeadlineViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
     originalSearchBarRect = _searchBar.frame;
    _searchBar.hidden = YES;
    _searchBarHeighConstraint.constant = 0.0;
    _tableviewTopContraint.constant = 0.0;
    
    NewsDataModel *newsDataModel = [NewsDataModel sharedInstance];
    [newsDataModel reloadData:^(BOOL isDataAvailable) {
        if (isDataAvailable) {
            defaultDataContentArray = [newsDataModel allNewsArticles];
            DLog(@"dataContent: %@", defaultDataContentArray);
            _transientDataSource = defaultDataContentArray;
            
            //Display content
            countOfRows = _transientDataSource.count;
            dispatch_async(dispatch_get_main_queue(), ^{
                [_headlineTableView reloadData];
            });
            
        }
    }];

}

- (IBAction)showSearchBar:(id)sender
{
    if ([_searchButton.title isEqualToString:@"Cancel"]) {
        _searchButton.title = @"Search";
        _transientDataSource = defaultDataContentArray;
        [_headlineTableView reloadData];
        
        _searchBar.hidden = YES;
        _searchBarHeighConstraint.constant = 0.0;
        _tableviewTopContraint.constant = 0.0;
        [_searchBar resignFirstResponder];
    }else{
        _searchButton.title = @"Cancel";
        _searchBar.text = @" ";
        _searchBar.hidden = NO;
        _searchBarHeighConstraint.constant = originalSearchBarRect.size.height;
        _tableviewTopContraint.constant = originalSearchBarRect.size.height;
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - search bar delegate
// called when text changes (including clear)
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    @autoreleasepool {
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        if (searchText.length > 0) {
            
            NSPredicate* predicate = [NSPredicate predicateWithFormat:@"%K contains[cd] %@", @"HEADLINE",  searchBar.text];
            NSArray *filteredArray = [_transientDataSource filteredArrayUsingPredicate:predicate];
            _transientDataSource = filteredArray;
            
        }
        else{
            _transientDataSource = defaultDataContentArray;
        }
        
        [_headlineTableView reloadData];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }
    
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
//    [self searchBar:searchBar textDidChange:nil];
    _searchButton.title = @"Search";
    _transientDataSource = defaultDataContentArray;
    [_headlineTableView reloadData];
    
    _searchBar.hidden = YES;
    _searchBarHeighConstraint.constant = 0.0;
    _tableviewTopContraint.constant = 0.0;
    [searchBar resignFirstResponder];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return _transientDataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"newsHeaderTableViewCell";
    NewsHeaderTableCell *cell = [_headlineTableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[NewsHeaderTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    NewsHeaderModel *headerModel = [_transientDataSource objectAtIndex:indexPath.row];
    cell.rowIndex = indexPath.row;
    cell.newsHeaderModel = headerModel;
    cell.headlineLabel.text = headerModel.HEADLINE;
    cell.dateLabel.text = headerModel.DATE;
    NSString *imagePath = [[NSBundle mainBundle] pathForResource:headerModel.IMAGENAME ofType:@"png"];
    cell.thumbnailImageView.image = [[UIImage alloc] initWithContentsOfFile:imagePath];
    cell.likeCountLabel.text = headerModel.LIKES_COUNT;
    if ([headerModel.SELECTION_STATE caseInsensitiveCompare:@"Un-Selected"] == NSOrderedSame) {
        [cell.heartButton setImage:[UIImage imageNamed:@"heartEmpty"] forState:UIControlStateNormal];
        cell.heartButton.selected = NO;
    }
    else{
        [cell.heartButton setImage:[UIImage imageNamed:@"heartFull"] forState:UIControlStateNormal];
        cell.heartButton.selected = YES;
    }
    
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIStoryboard *storyboard = nil;
    storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    DetailViewController *detailVC = [storyboard instantiateViewControllerWithIdentifier:@"DetailViewController"];
    
    NewsHeaderModel *headerModel = [_transientDataSource objectAtIndex:indexPath.row];
    detailVC.urlString = headerModel.WEBSITE;
    
    [self.navigationController pushViewController:detailVC animated:YES];
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}



@end
