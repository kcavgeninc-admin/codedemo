//
//  NewsHeaderTableCell.h
//  NewsReaderTestApp
//
//  Created by Danekar, Koustubh S. (Contractor) on 2/11/16.
//  Copyright © 2016 Danekar, Koustubh S. (Contractor). All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NewsHeaderModel.h"

@interface NewsHeaderTableCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel* headlineLabel;
@property (weak, nonatomic) IBOutlet UILabel* dateLabel;
@property (weak, nonatomic) IBOutlet UIImageView *thumbnailImageView;
@property (weak, nonatomic) IBOutlet UIButton *heartButton;
@property (weak, nonatomic) IBOutlet UILabel *likeCountLabel;

@property (assign, nonatomic) NewsHeaderModel *newsHeaderModel;
@property NSInteger rowIndex;

@end
