//
//  DetailViewController.m
//  NewsReaderTestApp
//
//  Created by Danekar, Koustubh S. (Contractor) on 2/11/16.
//  Copyright © 2016 Danekar, Koustubh S. (Contractor). All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController()<UIWebViewDelegate>

@end


@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
        
    [_detailWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:_urlString]]];
    
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request   navigationType:(UIWebViewNavigationType)navigationType {
    return YES;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(nullable NSError *)error
{
    DLog(@"Error in loading: %@", [error description]);
}

@end
