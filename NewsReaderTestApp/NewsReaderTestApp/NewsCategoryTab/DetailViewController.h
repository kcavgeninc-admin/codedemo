//
//  DetailViewController.h
//  NewsReaderTestApp
//
//  Created by Danekar, Koustubh S. (Contractor) on 2/11/16.
//  Copyright © 2016 Danekar, Koustubh S. (Contractor). All rights reserved.
//


@interface DetailViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIWebView *detailWebView;
@property (strong, nonatomic) NSString *urlString;

@end
