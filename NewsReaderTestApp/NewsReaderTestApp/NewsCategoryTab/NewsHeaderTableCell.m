//
//  NewsHeaderTableCell.m
//  NewsReaderTestApp
//
//  Created by Danekar, Koustubh S. (Contractor) on 2/11/16.
//  Copyright © 2016 Danekar, Koustubh S. (Contractor). All rights reserved.
//

#import "NewsHeaderTableCell.h"
#import "NewsDataModel.h"

@implementation NewsHeaderTableCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state

}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.imageView.frame = CGRectMake(0,0,70,70);
}

- (IBAction)changeHeart:(id)sender
{
    NewsDataModel *newsDataModel = [NewsDataModel sharedInstance];
    if ([sender isSelected]) {
        [sender setImage:[UIImage imageNamed:@"heartEmpty"] forState:UIControlStateNormal];
        _newsHeaderModel.LIKES_COUNT = [NSString stringWithFormat:@"%ld", [_newsHeaderModel.LIKES_COUNT integerValue] - 1];
        self.likeCountLabel.text = _newsHeaderModel.LIKES_COUNT;
        [newsDataModel decrementLikesCountOf:_newsHeaderModel AtIndex:self.rowIndex];
       
        [sender setSelected:NO];
        
    } else {
        [sender setImage:[UIImage imageNamed:@"heartFull"] forState:UIControlStateSelected];
        
        _newsHeaderModel.LIKES_COUNT = [NSString stringWithFormat:@"%ld", [_newsHeaderModel.LIKES_COUNT integerValue] + 1];
        self.likeCountLabel.text = _newsHeaderModel.LIKES_COUNT;
        [newsDataModel incrementLikesCountOf:_newsHeaderModel AtIndex:self.rowIndex];
        
         [sender setSelected:YES];
    }
}



@end
