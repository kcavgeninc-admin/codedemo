//
//  SynthesizeSingleton.h
//  KCFramework
//
//  Created by KC on 3/6/13.
//  Copyright (c) 2013 KC. All rights reserved.
//

#ifndef SYNTHESIZE_SINGLETON_FOR_CLASS

#import <objc/runtime.h>

/* Synthesize Singleton For Class
 *
 * Creates a singleton interface for the specified class with the following methods:
 *
 * + (MyClass*) sharedInstance;
 *
 *
 *
 * Usage:
 *
 * MyClass.h:
 * ========================================
 *	#import "SynthesizeSingleton.h"
 *
 *	@interface MyClass: SomeSuperclass
 *	{
 *		...
 *	}
 *	SYNTHESIZE_SINGLETON_FOR_CLASS_HEADER(MyClass);
 *
 *	@end
 * ========================================
 *
 *
 *	MyClass.m:
 * ========================================
 *	#import "MyClass.h"
 *
 *	@implementation MyClass
 *
 *	SYNTHESIZE_SINGLETON_FOR_CLASS(MyClass);
 *
 *	...
 *
 *	@end
 * ========================================
 *
 *
 */

#define SYNTHESIZE_SINGLETON_FOR_CLASS_HEADER(SS_CLASSNAME)             \
\
+ (SS_CLASSNAME*) sharedInstance;                                       \


#define SYNTHESIZE_SINGLETON_FOR_CLASS(SS_CLASSNAME)                    \
\
static SS_CLASSNAME* sharedInstance = nil;                              \
\
\
+ (id) sharedInstance {                                                 \
static dispatch_once_t onceToken;                                   \
dispatch_once(&onceToken, ^{                                        \
if (sharedInstance == NULL){                                    \
sharedInstance = [[super allocWithZone:NULL] init];         \
}                                                               \
});                                                                 \
return sharedInstance;                                              \
}                                                                       \
\
+ (id) allocWithZone:(NSZone *)paramZone {                              \
@synchronized(self)	{                                               \
if(sharedInstance == nil) {                                     \
[self sharedInstance];                                      \
}                                                               \
}                                                                   \
return sharedInstance;                                              \
}                                                                       \
\
- (id) copyWithZone:(NSZone *)paramZone {                               \
return self;                                                        \
}                                                                       \

#endif /* SYNTHESIZE_SINGLETON_FOR_CLASS */
